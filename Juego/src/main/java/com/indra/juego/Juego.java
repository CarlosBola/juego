/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

/**
 *
 * @author Crucigrama
 */
public abstract class Juego {

    private Tablero tablero;
    
    public Juego(Tablero tablero){
        this.tablero = tablero;
    }
    
    public abstract void IniciarJuego();
    
    public Tablero getTablero() {
        return tablero;
    }

    public void setTablero(Tablero tablero) {
        this.tablero = tablero;
    }
}
