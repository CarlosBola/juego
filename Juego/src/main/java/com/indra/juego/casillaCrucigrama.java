package com.indra.juego;


import com.indra.juego.Casilla;

public class casillaCrucigrama extends Casilla {
    casillaCrucigrama casillaSiguiente;
    int numeroDePregunta;
    boolean orientacion;
    
    public casillaCrucigrama(){
        super(' ');//super((char)0x00a4);
        setCasillaSiguiente(null);
        numeroDePregunta = 0;
        orientacion = false;
    }

    public casillaCrucigrama(char caracter) {
        super(caracter);
        setCasillaSiguiente(null);
        numeroDePregunta = 0;
        orientacion = false;
    }

    public casillaCrucigrama(char caracter, casillaCrucigrama casillaSiguiente){
        super(caracter);
        setCasillaSiguiente(casillaSiguiente);
        numeroDePregunta = 0;
        orientacion = false;
    }
            
            
    public casillaCrucigrama getCasillaSiguiente() {
        return casillaSiguiente;
    }

    public void setCasillaSiguiente(casillaCrucigrama casillaSiguiente) {
        this.casillaSiguiente = casillaSiguiente;
    }
    
    public int getNumeroDePregunta(){
        return numeroDePregunta;
    }
    
    public void setNumeroDePregunta(int numero){
        this.numeroDePregunta = numero;
    }
    
    public boolean getOrientacion(){
        return orientacion;
    }
    
    public void setOrientacion(boolean orientacion){
        this.orientacion = orientacion;
    }
    
}
