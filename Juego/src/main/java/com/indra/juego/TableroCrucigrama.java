package com.indra.juego;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableroCrucigrama extends Tablero {

    final boolean VERTICAL = true;
    final boolean HORIZONTAL = false;
    int limite;
    int carga;

    TableroCrucigrama(int filas, int columnas, int limite) {
        super(filas, columnas);
        this.limite = limite;
        carga = 0;
        llenarTablero();
    }

    @Override
    public void validarTamaño() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 /**
  * Imprime el tablero del juego
  */
    @Override
    public void mostrarTablero() {
        int numero;
        for (int fila = 0; fila < filas; fila++) {
            System.out.println("");
            for (int columna = 0; columna < columnas; columna++) {
                casillaCrucigrama casilla = getCasillaEnCuadricula(fila, columna);
                numero = casilla.getNumeroDePregunta();
                if (numero > 9) {
                    System.out.print(" |" + casilla.getNumeroDePregunta());
                } else if (numero > 0) {
                    System.out.print(" | " + casilla.getNumeroDePregunta());
                } else if (casilla.isEsParteDeSolucion()) {
                    System.out.print(" |" + " ?");
                } else {
                    System.out.print(" |" + "  ");
                }
            }
            System.out.print("|");
        }
        System.out.println("");
    }
 /**
  * Muestra la solucion 
  */
    @Override
    public void mostrarTableroSolucion() {
        for (int fila = 0; fila < filas; fila++) {
            System.out.println("");
            for (int columna = 0; columna < columnas; columna++) {
                casillaCrucigrama casilla = getCasillaEnCuadricula(fila, columna);
                System.out.print(" " + casilla.getCaracter() + " ");
            }
        }
        System.out.println("");
    }

    /**
     * Declarar la matriz de casillas vacias
     */
    @Override
    public void llenarTablero() {
        for (int columna = 0; columna < columnas; columna++) {
            for (int fila = 0; fila < filas; fila++) {
                setCasillaEnCuadricula(fila, columna, new casillaCrucigrama());
            }
        }
    }

    /**
     * Asigna a las palabras su posición de tablero
     *
     * @param palabras lista de las iniciales de todas las pablabras
     */
    public void llenarTablero(List<casillaCrucigrama> palabras) {
        casillaCrucigrama primerLetra = palabras.get(0);
        colocarPrimerPalabra(primerLetra);
        
        boolean orientacion = VERTICAL;
        //incia con vertical
        if(limite > 1){
        int indicePalabraSig = buscaCruzarPalabra(primerLetra, palabras, orientacion);
        // devuelve el indice de la nueva palabra y la nueva orientacion
        for (int i = 1; i < palabras.size(); i++) {
            if(carga != limite){
                if (indicePalabraSig > 0) { // indica que la siguiente orientacion es vertical
                    indicePalabraSig = buscaCruzarPalabra(palabras.get(indicePalabraSig), palabras, orientacion);
                } else if (indicePalabraSig < 0) { // indica que la siguiente orientacion es horizontal
                    indicePalabraSig = indicePalabraSig * -1;
                    indicePalabraSig = buscaCruzarPalabra(palabras.get(indicePalabraSig), palabras, !orientacion);
                } else {
                    //  System.out.println("No hubo cruzas");
                    break;
                }
            }
        }
        }
    }

    /**
     * Primer referencia de la lista se coloca de forma horizontal y centrada en
     * la matriz
     *
     * @param cabezita Referencia inicial de la palabra a colocar
     */
    private void colocarPrimerPalabra(casillaCrucigrama cabezita) {//se asigna posicion 1er palabra
        int casillas, alojamiento;
        int tamañoDePalabra = 0;

        /* Coordenada 'y' */
        alojamiento = obtenCoordenadaDeAlojamiento();
        /* Coordenada 'x' */
        tamañoDePalabra = obtenTamañoDePalabra(cabezita);
        casillas = obtenCoordenadaInicialEn(columnas, tamañoDePalabra);
        /* Asignación de coordenadas para la cadena de casillas */
        asignaCoordenadasPalabra(cabezita, alojamiento, casillas, HORIZONTAL);
    }

    /**
     * Obtener tamaño de la palabra a partir de su primer nodo
     *
     * @param nodo Nodo inicial de la palabra
     * @return
     */
    private int obtenTamañoDePalabra(casillaCrucigrama nodo) {
        int tamaño = 0;
        do {
            tamaño++;
            nodo = nodo.getCasillaSiguiente();
        } while (nodo != null);
        return tamaño;
    }

    /**
     * Obtiene la coordenada donde empezara la palabra, puede ser de manera
     * horizontal o vertical.
     *
     * @param tamañoDePalabra
     * @param maximoDeEspacios Es la cantidad de casillas disponibles de una de
     * una fila o columna.
     * @return
     */
    private int obtenCoordenadaInicialEn(int maximoDeEspacios, int tamañoDePalabra) {
        int coordenadaInicio = 0;
        int casillasLibres = maximoDeEspacios - tamañoDePalabra;
        if (casillasLibres != 0) {
            if (casillasLibres % 2 == 0) {
                coordenadaInicio = (casillasLibres / 2);
            } else {
                coordenadaInicio = (casillasLibres - 1) / 2;
            }
        }
        return coordenadaInicio;
    }

    /**
     * Obtener la coordenada de "y" para la primer palabra
     *
     * @return
     */
    private int obtenCoordenadaDeAlojamiento() {
        int coordenadaAlojamiento;
        if (filas % 2 == 0) {
            coordenadaAlojamiento = (filas / 2) - 1;
        } else {
            coordenadaAlojamiento = (filas - 1) / 2;
        }
        return coordenadaAlojamiento;
    }

    /**
     * Asigna las coordenadas a cada nodo, crea una nueva casilla con la misma
     * informacion que el nodo y lo coloca en la matriz de casillas
     *
     * @param nodo Nodo inicial de palabra
     * @param fila Coordenada Y
     * @param columna Coordenada X
     * @param orientacion Forma de insertar la palabra:Vertical(true) u
     * Horizontal(false),
     */
    private void asignaCoordenadasPalabra(casillaCrucigrama nodo, int fila, int columna, boolean orientacion) {

        char copiaCaracter;
        int copiaNumeroDePregunta;
        int numero;
        /* Aqui se ocupa la carga para llevar un conteo de las palabras (respuestas)
         * que hay en el tablero. Este numero servira para el juego, donde se muestra
         * la pregunta de la casilla.
         * Se ubica fuera del do-while, por que solo se busca poner el indicador en
         * la cabeza del nodo*/
        nodo.setNumeroDePregunta(++carga);

        do {
            nodo.setColumna(columna);
            nodo.setFila(fila);
            // System.out.println("Letra ="+ nodo.getCaracter()+"["+ columna +", "+ fila +"]");
            nodo.setEsParteDeSolucion(true);
            nodo.setOrientacion(orientacion);

            copiaCaracter = nodo.getCaracter();
            copiaNumeroDePregunta = nodo.getNumeroDePregunta();

            casillaCrucigrama copiaCasilla = new casillaCrucigrama(copiaCaracter);
            copiaCasilla.setColumna(columna);
            copiaCasilla.setFila(fila);
            copiaCasilla.setEsParteDeSolucion(true);
            copiaCasilla.setOrientacion(orientacion);

            /* Como se pierde el numero de pregunta a la hora de imprimir el tablero
             * se tiene que copiar el numero de la casilla en cuadricula (si tiene)
             * al nodo por asignar. */
            numero = getCasillaEnCuadricula(fila, columna).getNumeroDePregunta();
            if (numero > 0) {
                copiaNumeroDePregunta = numero;
            }
            copiaCasilla.setNumeroDePregunta(copiaNumeroDePregunta);

            setCasillaEnCuadricula(fila, columna, copiaCasilla);

            if (orientacion) {
                fila++;
            } else {
                columna++;
            }

            nodo = nodo.getCasillaSiguiente();
        } while (nodo != null);
    }

    /**
     * Compara letra por palabra compara la letra de la primer palabra con la
     * segunda palabra
     *
     * @param casillaBuscando Letra a buscar (inicial de la palabra anterior)
     * @param palabra Referencia a la palabra siguiente
     * @param orientacion
     * @return Si se asigno una palabra a la matriz
     */
    private boolean busquedaDeLetraPorPalabra(casillaCrucigrama casillaBuscando, casillaCrucigrama palabra, boolean orientacion) {
        char busqueda = casillaBuscando.getCaracter();
        casillaCrucigrama letraSiguiente = palabra;
        int contador = 0;
        int distanciaDesdeInicio = -1;
        while (letraSiguiente != null) {
            if (busqueda == letraSiguiente.getCaracter()) {
                distanciaDesdeInicio = contador;
                // System.out.println("Coincidencia en: " + distanciaDesdeInicio);
                break;
            }
            contador++;
            letraSiguiente = letraSiguiente.getCasillaSiguiente();
        }
        if (distanciaDesdeInicio != -1) {
            //Hubo una coincidencia, ahora hay que verificar si cabe
            return sePuedeIngresarNuevaPalabra(distanciaDesdeInicio,
                    casillaBuscando,
                    palabra,
                    orientacion);
        } else {
            //  System.out.println("No hubo coincidencia de "+busqueda+" en la palabra");
        }
        return false;
    }

    /**
     * Verifica que la palabra que se quiere ingresar no se haya ingresado antes
     * y que tenga espacio para ser ingresada a la matriz
     *
     * @param distanciaOrigenNuevaPalabra Espacios de la interseccion al inicio
     * de la nueva palabra
     * @param referenciaCruce Casilla donde encontro el cruce(palabra anterior)
     * @param referenciaNuevaPalabra Casilla de la nueva palabra (cabeza)
     * @param orientacion
     * @return Cumple con las condiciones(no esta repetida y si cabe)
     */
    private boolean sePuedeIngresarNuevaPalabra(int distanciaOrigenNuevaPalabra,
            casillaCrucigrama referenciaCruce,
            casillaCrucigrama referenciaNuevaPalabra,
            boolean orientacion) {

        int coordenadaVariableInicialNuevaPalabra;
        int coordenadaEstaticaInicialNuevaPalabra;
        int coordenadaCruceVariable;
        int coordenadaCruceEstatica;
        int numeroTotalDeCasillas;
        boolean esIntegrable = false;

        if (orientacion) { // true si la palabra a ingresar va en vertical
            coordenadaCruceVariable = referenciaCruce.getFila();
            coordenadaCruceEstatica = referenciaCruce.getColumna();
            numeroTotalDeCasillas = filas;
        } else { // false es una a palabra a ingresar horizontal
            coordenadaCruceVariable = referenciaCruce.getColumna();
            coordenadaCruceEstatica = referenciaCruce.getFila();
            numeroTotalDeCasillas = columnas;
        }
        coordenadaEstaticaInicialNuevaPalabra = coordenadaCruceEstatica;
        coordenadaVariableInicialNuevaPalabra = coordenadaCruceVariable - distanciaOrigenNuevaPalabra;
        /* <- Se obtuvo la coordenada donde se pondra la palabra*/

        int recorridoDePalabraNueva = coordenadaVariableInicialNuevaPalabra;
        int espaciosDespuesDelCruce = numeroTotalDeCasillas
                - obtenTamañoDePalabra(referenciaNuevaPalabra) - recorridoDePalabraNueva;
        int espaciosAntesDelCruce = recorridoDePalabraNueva;
        /* Se verifica que la palabra horizontal/verticalmente en el cruce -> */
        if (espaciosAntesDelCruce >= 0) { // caben las letras de la palabra nueva antes del cruce
            if (espaciosDespuesDelCruce >= 0) { // caben las letras de la palabra nueva despues del cruce
                /* Verificar que las casillas no esten ocupadas:
                 *  1) no esta ocupada: isEsParteDeSolucion = false
                 *  2) esta ocupada pero tiene el mismo caracter */
                esIntegrable = verificaCasillas(coordenadaVariableInicialNuevaPalabra,
                        coordenadaEstaticaInicialNuevaPalabra,
                        referenciaNuevaPalabra,
                        orientacion);
                if (esIntegrable) {
                    if (orientacion) { // formato veritcal fila es variable, columna es estatica, orientacion es true
                        asignaCoordenadasPalabra(referenciaNuevaPalabra,
                                coordenadaVariableInicialNuevaPalabra,
                                coordenadaEstaticaInicialNuevaPalabra,
                                orientacion);
                    } else { // formato horzontal columna es variable, fila es estatica, orientacion es true
                        asignaCoordenadasPalabra(referenciaNuevaPalabra,
                                coordenadaEstaticaInicialNuevaPalabra,
                                coordenadaVariableInicialNuevaPalabra,
                                orientacion);
                    }
                }

            } // la palabra no cabe despues
        } // la palabra no cabe antes
        return esIntegrable;
    }

    /**
     * Verifica si estan disponibles las casillas y si estan ocupadas que
     * coincidan sus caracteres
     *
     * @param coordenadaVariable Dependiendo la orientacion si es vertical=y ,
     * horizontal=x, de la nueva palabra
     * @param coordenadaEstatica Dependiendo la orientacion si es vertical=x ,
     * horizontal=y, de la nueva palabra
     * @param referenciaPalabraNueva Referencia inicial de la palabra nueva
     * (cabeza)
     * @param orientacion
     * @return Si encaja la palabra
     */
    private boolean verificaCasillas(int coordenadaVariable, int coordenadaEstatica,
            casillaCrucigrama referenciaPalabraNueva,
            boolean orientacion) {
        int fila, columna;
        casillaCrucigrama casilla, iterador;
        boolean esPalabraInsertable = false;

        if (orientacion) {
            fila = coordenadaVariable;
            columna = coordenadaEstatica;
        } else {
            fila = coordenadaEstatica;
            columna = coordenadaVariable;
        }

        if (!referenciaPalabraNueva.isEsParteDeSolucion()) {
            iterador = referenciaPalabraNueva;
            while (iterador != null) {
                casilla = getCasillaEnCuadricula(fila, columna);
                if (!casilla.isEsParteDeSolucion()) { // La casilla esta libre
                    esPalabraInsertable = true;
                } else { // La casilla esta  ocupada por una letra, checar si son las misma
                    if (iterador.getCaracter() == casilla.getCaracter()) {
                        esPalabraInsertable = true;
                    } else {
                        esPalabraInsertable = false;
                    }
                }
                if (!esPalabraInsertable) {
                    break;
                }

                iterador = iterador.getCasillaSiguiente();
                if (orientacion) {
                    fila++;
                } else {
                    columna++;
                }
            }
        }
        return esPalabraInsertable;
    }

    /**
     *Busca coincidencias de cada letra de la palabra anterior,comienza con las iniciales de la
     * anterior y la actual. Hace un recorrido por la palabra siguiente y despues por toda lista hasta 
     * que encuentre una coincidencia.
     * @param palabra Inicial de la palabra anterior ya colocada
     * @param palabras Lista de las referencias de las palabras a buscar (cabezas)
     * @param orientacion
     * @return Indice de la lista de la palabra en la lsita de palabras, 
     * Si es negativo la siguiente palabra a cruzar debe ser horizontal y viceversa
     */
    private int buscaCruzarPalabra(casillaCrucigrama palabra, List<casillaCrucigrama> palabras, boolean orientacion) {
        boolean cruza = false;
        int numeroPalabra = 0;
        casillaCrucigrama primerLetra = palabra;

        for (int z = 1; z < palabras.size(); z++) {
            //System.out.println("..." + z);
            // busca coincidencia de las letras de la primera palabra con todas las demás palabras

            while (primerLetra != null) {
                cruza = busquedaDeLetraPorPalabra(primerLetra, palabras.get(z), orientacion);
                if (cruza) {
                    break;
                }
                primerLetra = primerLetra.getCasillaSiguiente();
            }
            if (cruza) {
                numeroPalabra = z;
                if (orientacion) {
                    numeroPalabra = numeroPalabra * -1;
                }
                break;
            }
            primerLetra = palabra;
        }
        return numeroPalabra;
    }
}
