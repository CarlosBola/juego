package com.indra.juego;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.*;

public class archivos {
/**
 * Diccionario, lee y separa la palabra de la pregunta con el caracter "="
 * @return 
 */
    public List<Map> leerArchivo() {
        List<Map> listaMapas = new ArrayList();
        String nombreArchivo = "src/main/java/Docs/dic.txt";

        File archivo = new File(nombreArchivo);
        BufferedReader entrada;
        try {
            entrada = new BufferedReader(new FileReader(archivo));
            String lectura = entrada.readLine();

            while (lectura != null) {
                String UTF8Str = new String(lectura.getBytes(), "UTF-8");
                String[] parts = UTF8Str.split("=");
                Map relacionPreguntas = new HashMap();
                relacionPreguntas.put(parts[0], parts[1]);//parts[0]: Palabra(key), parts[1]: pregunta(Value)
                listaMapas.add(relacionPreguntas);
                lectura = entrada.readLine();

            }
            entrada.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return listaMapas;
    }
}
