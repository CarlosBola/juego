/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author w10
 */
public class Palabras {

    private static List<Map> listaPalabras = new archivos().leerArchivo();
    protected List<Map> palabrasAlAzar = new ArrayList();
    protected String palabra;
    protected String respuesta;
/**
 * Selecciona las palabras al azar, comparacion tamaño minimo de fila o columna
 * agrega a la listaPalabras 
 * @param maximo tamaño limite
 * @param cantidad de palabras
 * @return 
 */
    public List<Map> devolverPalabras(int maximo, int cantidad) {
        //System.out.println("En devolverPalabras...");
        SecureRandom sr = new SecureRandom();
        Map mapa = new HashMap();
    
        for (int i = 0; i < maximo; i++) {
            int checksum = 0;
            mapa = listaPalabras.get(sr.nextInt(listaPalabras.size()));
            palabra = mapa.keySet().toString();
            if ((palabra.length() - 2) <= cantidad) {
                if (!palabrasAlAzar.isEmpty()) {
                    for (int j = 0; j < i; j++) {
                        if (!mapa.keySet().equals(palabrasAlAzar.get(j).keySet())) {
                            checksum++;
                        }
                    }
                    if (checksum == i) { //hay tantas palabras i unicas
                        // por lo tanto mapa no se repitio  
                        palabrasAlAzar.add(mapa);
                        //System.out.println(mapa.keySet().toString());
                    } else {
                        i = i >= 0 ? --i : 0;
                    }
                } else {
                    palabrasAlAzar.add(mapa);
                }
            } else {
                i = i >= 0 ? --i : 0;
            }
        }
        return palabrasAlAzar;
    }

    public void imprimePalabrasEscogidas(List<Map> palabras) {
        Map mapa = new HashMap();
        for (Map palabra : palabras) {
            mapa = palabra;
            respuesta = mapa.keySet().toString();
            System.out.println("palabra: " + respuesta + " Pregunta: " + mapa.values());
        }
    }
/**
 * Se ordena de mayor a menor con el metodo burbuja
 * @param palabrasAlAzar de la lista random del metodo devolverPalabraAlAzar
 */
    private void ordenar(List<Map> palabrasAlAzar) {
        String respuesta = null;
        int tamañoPalabraActual;
        int tamañoPalabraNuevo;
        Map comodin = new HashMap();

        boolean flag = true;

        while (flag) {
            flag = false;
            for (int i = 0; i < palabrasAlAzar.size() - 1; i++) {
                tamañoPalabraNuevo = palabrasAlAzar.get(i + 1).keySet().toString().length();
                respuesta = palabrasAlAzar.get(i).keySet().toString();
                tamañoPalabraActual = respuesta.length();
                if (tamañoPalabraActual < tamañoPalabraNuevo) { // comparar tamaño de palabras
                    comodin = palabrasAlAzar.get(i);
                    palabrasAlAzar.set(i, palabrasAlAzar.get(i + 1));
                    palabrasAlAzar.set(i + 1, comodin);
                    flag = true;
                }
            }
        }
    }
/**
 * 
 * @param palabrasAlAzar
 * @return 
 */
    public List<casillaCrucigrama> instanciarPalabras(List<Map> palabrasAlAzar) {

        ordenar(palabrasAlAzar);
        /* 1. Manejar una lista de casillasCrucigrama
         * 2. Instanciar las casillas unicamente con caracter, sin siguiente.
         * 3. Guardar las instancias de la clase en la lista.
         * 4. Enlazar las instancias.
         */
        List<casillaCrucigrama> cabezas = new ArrayList();
        List<casillaCrucigrama> listaLetras;
        for (int y = 0; y < palabrasAlAzar.size(); y++) {
            listaLetras = new ArrayList();
            respuesta = palabrasAlAzar.get(y).keySet().toString();
            for (int z = 1; z < respuesta.length() - 1; z++) {
                casillaCrucigrama letra = new casillaCrucigrama(respuesta.charAt(z));
                listaLetras.add(letra);
            }
            cabezas.add(listaLetras.get(0));
            for (int z = 0; z < listaLetras.size() - 1; z++) {
                casillaCrucigrama casillaSig = listaLetras.get(z + 1);
                listaLetras.get(z).setCasillaSiguiente(casillaSig);
            }
        }
        return cabezas;
    }

    private void imprimirPalabras(List<casillaCrucigrama> cabezas) {
        System.out.println("");
        for (int z = 0; z < cabezas.size(); z++) {
            casillaCrucigrama cabezita = cabezas.get(z);
            while (cabezita.getCasillaSiguiente() != null) {
                //   System.out.print("["+ cabezita.getCaracter() +"]");
                cabezita = cabezita.getCasillaSiguiente();
            }
            System.out.print("[" + cabezita.getCaracter() + "]");
            System.out.println("");
        }
    }
}
