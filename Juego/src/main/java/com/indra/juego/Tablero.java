/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.util.List;

/**
 *
 * @author w10
 */
public abstract class Tablero {

    protected int filas;
    protected int columnas;
    private Casilla[][] cuadricula;
    private int numeroPalabras;
    private List<List<Casilla>> soluciones;
    
    public Tablero(int filas, int columnas){
        this.filas = filas;
        this.columnas = columnas;
        cuadricula = new Casilla[filas][columnas];
    }
    
    public abstract void llenarTablero();
    
    public abstract void validarTamaño();
    
    public abstract void mostrarTablero();
    
    public abstract void mostrarTableroSolucion();
    
    public int getFilas() {
        return filas;
    }

    public void setFilas(int filas) {
        this.filas = filas;
    }

    public int getColumnas() {
        return columnas;
    }

    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }

    public Casilla[][] getCuadricula() {
        return cuadricula;
    }

    public void setCuadricula(Casilla[][] casillas) {
        this.cuadricula = casillas;
    }
    
    public casillaCrucigrama getCasillaEnCuadricula(int fila, int columna){
        return (casillaCrucigrama) cuadricula[fila][columna];
    }
    
public void setCasillaEnCuadricula(int fila, int columna, casillaCrucigrama nodo){
        cuadricula[fila][columna] = nodo;
    }

    public int getNumeroPalabras() {
        return numeroPalabras;
    }

    public void setNumeroPalabras(int numeroPalabras) {
        this.numeroPalabras = numeroPalabras;
    }
}
