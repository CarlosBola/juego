/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author w10
 */
public class JuegoCrucigrama extends Juego {

    List<casillaCrucigrama> cabezas;
    List<Map> listaDeMapas;
    
    public JuegoCrucigrama(Tablero tablero, List<casillaCrucigrama> cabezas, List<Map> listaDeMapas) {
        super(tablero);
        this.cabezas = cabezas;
        this.listaDeMapas = listaDeMapas;
    }

    @Override
    public void IniciarJuego() {
        mostrarPreguntas();
        
    }
    
    private void mostrarPreguntas(){
        // Mostrar preguntas
        List<casillaCrucigrama> referenciasHorizontales, referenciasVerticales;
        referenciasHorizontales = extraeReferenciasUtiles(false);
        referenciasVerticales = extraeReferenciasUtiles(true);
        //  1) ordenar la lista de menor a mayor
        acomodaRespuestas(referenciasHorizontales);
        acomodaRespuestas(referenciasVerticales);
        int[] numeracionHorizontales = guardaNumerosDePregunta(referenciasHorizontales);
        int[] numeracionVerticales = guardaNumerosDePregunta(referenciasVerticales);
        //  2) encontrar la pregunta
        String[] respuestasHorizontales = recuperaRespuestas(referenciasHorizontales);
        System.out.println("\n<<< Horizontales >>>");
        imprimePreguntas(respuestasHorizontales, numeracionHorizontales);
        
        String[] respuestasVerticales = recuperaRespuestas(referenciasVerticales);
        System.out.println("\n^^^ Verticales ^^^");
        imprimePreguntas(respuestasVerticales, numeracionVerticales);
    }

     /**
      * Obtener lista de las cabezas de palabras que se encuentran el tablero 
      * @param tipo Si es horizontal o vertical, True=vertical, false=horizontal
      * @return Lista de palabras(respuestas)
      */
    private List<casillaCrucigrama> extraeReferenciasUtiles(boolean tipo){
        List<casillaCrucigrama> referenciasUtilizadas = new ArrayList();
        for(casillaCrucigrama cabeza : cabezas){
            if(cabeza.isEsParteDeSolucion()){
                if(cabeza.getOrientacion() == tipo){ // si la orientacion es vertical es true
                    referenciasUtilizadas.add(cabeza);
                }
            }
        }
        return referenciasUtilizadas;
    }
     /**
      * AComoda de mayor a menor de acuerdo a su indice las respuestas
      * @param respuestas Lista de respuestas
      */
    private void acomodaRespuestas(List<casillaCrucigrama> respuestas){
        boolean flag = true;
        casillaCrucigrama temp;
        
        while(flag){
            flag = false;
            for(int i = 0; i < (respuestas.size() - 1); i++){
                if(respuestas.get(i).getNumeroDePregunta() > respuestas.get(i+1).getNumeroDePregunta()){
                    temp = respuestas.get(i);
                    respuestas.set(i, respuestas.get(i+1));
                    respuestas.set(i+1, temp);
                    flag = true;
                }
            }
        }
    }
 /**
  * Extrar de la lista de respuestas el indice 
  * @param respuestas 
  * @return Orden de las respuestas
  */         
    private int[] guardaNumerosDePregunta(List<casillaCrucigrama> respuestas){
        int[] orden = new int[respuestas.size()];
        int i = 0;
        for(casillaCrucigrama respuesta : respuestas){
            orden[i] = respuesta.getNumeroDePregunta();
            i++;
        }
        return orden;
    }
     /**
      * Conversion de la lista de nodos a String
      * @param referencias
      * @return Respuestas en forma de String
      */
    private String[] recuperaRespuestas(List<casillaCrucigrama> referencias){
        int tamaño = referencias.size();
        String[] respuestas = new String[tamaño];
        StringBuilder palabra;
        
        for(int i = 0; i < referencias.size(); i++){
            palabra = new StringBuilder();
            casillaCrucigrama nodo = referencias.get(i);
            while(nodo != null){
                palabra.append(nodo.getCaracter());
                nodo = nodo.getCasillaSiguiente();
            }
            respuestas[i] = palabra.toString();
        }
        return respuestas;
    }
     /**
      * Transferencia de respuestas-preguntas de una lista de mapas a un mapa 
      * @return Mapa de respuestas=llaves y preguntas=valores
      */
    private Map extraeMapa(){
        Map mapaPreguntas = new HashMap<String,String>();     
        StringBuilder builder;
        //System.out.println("");
        for(Map mapa : listaDeMapas){
            String respuesta = mapa.keySet().toString();
            builder = new StringBuilder(respuesta);
            builder.deleteCharAt(0);
            builder.deleteCharAt(respuesta.length()-2);
            respuesta = builder.toString();
            //System.out.print(respuesta);
            
            String pregunta = mapa.values().toString();
            builder = new StringBuilder(pregunta);
            builder.deleteCharAt(0);
            builder.deleteCharAt(pregunta.length()-2);
            pregunta = builder.toString();
            //System.out.println(": "+pregunta);
            
            mapaPreguntas.put(respuesta, pregunta);
        }
        return mapaPreguntas;
    }
     /**
      * Imprimir las preguntas
      * @param llaves
      * @param numeracionDePreguntas 
      */
    private void imprimePreguntas(String[] llaves, int[] numeracionDePreguntas){
        Map mapa = extraeMapa();
        String pregunta;
        
        for(int i = 0; i < llaves.length; i++){
            pregunta = (String) mapa.get(llaves[i]);
            System.out.println(numeracionDePreguntas[i] +"- "+pregunta);
        }
    }
    
    public List<casillaCrucigrama> getListaDeReferencias(){
        return cabezas;
    }
    
    public void setListaDeReferencias(List<casillaCrucigrama> referencias){
        this.cabezas = referencias;
    }
    
}
