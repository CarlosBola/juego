package com.indra.juego;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author w10
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingresa el número de columnas: ");
        int columnas = scanner.nextInt();
        System.out.println("Ingresa el número de filas: ");
        int filas = scanner.nextInt();
        int tamañoLista = 0;
        
        int longitud = columnas<filas?columnas:filas;
        
        System.out.println("Máximo de palabras en crucigrama: "+(longitud-1));
        System.out.println("Introduce el numero maximo de palabras que quieres ver en el crucigrama: ");
        int limite = scanner.nextInt();
        limite = limite>(longitud-1) ? longitud-1 : limite;
        limite = limite<1 ? 1 : limite;
        if(tamañoLista < 4){
            tamañoLista = (longitud*2) - 1;
        } else {
            tamañoLista=limite*30;
            tamañoLista= tamañoLista>263?263:tamañoLista;
        }
        
        Palabras palabras = new Palabras();
        List<Map> palabrasAleatorias = new ArrayList(palabras.devolverPalabras(tamañoLista, longitud));
        //palabras.imprimePalabrasEscogidas(palabrasAleatorias);       
        List<casillaCrucigrama> cabezas = palabras.instanciarPalabras(palabrasAleatorias);
        TableroCrucigrama tablero = new TableroCrucigrama(filas, columnas, limite);
        // estas llamadas a metodos deberan ir en juego
        tablero.llenarTablero(cabezas);
        tablero.mostrarTableroSolucion();
        tablero.mostrarTablero();
        JuegoCrucigrama juego = new JuegoCrucigrama(tablero, cabezas, palabrasAleatorias);
        juego.IniciarJuego();
    }

}
